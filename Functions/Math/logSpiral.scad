/**
 * Genera los puntos de una espiral logarítmica.
 *
 * Ejemplo:
 *
 * ```openscad
 * module _(n)
 * {
 *     translate(logSpiral(n, -200, 40, 0.5, 3, 0 ))
 *     {
 *         cube([ 3, 0.01, 1 ], center = true);
 *     }
 * }
 *
 * for (n = [ 0 : 1 / 180 : 1 ])
 * {
 *     hull()
 *     {
 *         _(n);
 *         _(n + 1 / 180);
 *     }
 * }
 * ```
 *
 * @param {Float}   value   Valor sobre la espiral logarítmica para calcular sus coordenadas.
 * @param {Float}   height  Altura de la espiral (eje Z).
 * @param {Float}   radius  Radio de la espiral.
 * @param {Integer} turns   Número de vueltas que tendrá la espiral.
 * @param {Float}   falloff Factor de caída de la espiral.
 * @param {Float}   start   Ángulo inicial de la espiral.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Math/logSpiral.scad
 */
function logSpiral(value, height, radius = 24, falloff = 0.75, turns = 5, start = 0) = let(
        _angle = start + value * turns * 360 * sign(turns),
        _fn    = falloff < 0
            ? pow(max(0, 1 - value), 1 / abs(min(-1, falloff - 1)))
            : pow(max(0, 1 - value), max(1, falloff + 1))
    ) [ (radius * _fn) * cos(_angle), (radius * _fn) * sin(_angle), value * height ];
