/**
 * Módulo que incluye algunas funciones para trabajar con vectores.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Math/vectors.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
/**
 * Devuelve el ángulo entre 2 vectores.
 *
 * @param {Float[]} vector1 Vector 1 de entrada.
 * @param {Float[]} vector2 Vector 2 de entrada.
 *
 * @return {Float[]} Vector del centroide.
 */
function anglev(vec1,vec2) = acos(dot(vec1, vec2) / (norm(vec1) * norm(vec2)));

/**
 * Devuelve el centroide de dos vectores.
 *
 * @param {Float[]} vector1 Vector 1.
 * @param {Float[]} vector2 Vector 2.
 * @param {Float}   ratio   Relación entre los módulos de ambos vectores.
 *
 * @return {Float[]} Vector del centroide.
 */
function centroid(vector1, vector2, ratio) = vector1 * ratio + vector2 * (1 - ratio);

/**
 * Devuelve la distancia entre dos puntos.
 *
 * @param {Float[]} point1 Punto 1.
 * @param {Float[]} point2 Punto 2.
 *
 * @return {Float} Distancia entre los puntos.
 */
function distance(vec1, vec2) = norm(vec1 - vec2);

/**
 * Devuelve el producto punto de dos vectores.
 *
 * @param {Float[]} vec1 Vector 1 de entrada.
 * @param {Float[]} vec2 Vector 2 de entrada.
 *
 * @return {Float} Producto punto.
 */
function dot(vec1, vec2) = len(vec1) == 2
    ? vec1[0] * vec2[0] + vec1[1] * vec2[1]
    : vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2];

/**
 * Convierte a sistema cilíndrico los valores del sistema esférico especificados.
 *
 * @param {Float} radius Valor del radio.
 * @param {Float} angle  Valor del ángulo en grados.
 * @param {Float} z      Valor del eje Z.
 *
 * @return {Float[]} Vector del sistema cilíndrico.
 */
function toCylindrical(radius, angle, z) = [ radius * cos(angle), radius * sin(angle), z ];

/**
 * Devuelve el vector unitario del vector especificado.
 *
 * @param {Float[]} vector Vector de entrada.
 *
 * @return {Float[]} Vector del centroide.
 */
function uvec(vector) = vector / norm(vector);
