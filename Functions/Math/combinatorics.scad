/**
 * Diversas funciones usadas en combinatoria.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Math/combinatorics.scad
 * @license CC-BY-NC-4.0
 */

/**
 * Calcula el coeficiente binomial.
 *
 * @param {Float} n Número de elementos totales.
 * @param {Float} k Número de elementos en cada grupo.
 */
function binomialCoefficient(n, k) = k == 0 || n == k
    ? 1
    : factorial(n) / (factorial(k) * factorial(n - k));

/**
 * Calcula el factorial de un número.
 *
 * @param {Float} n Número para calcular su factorial.
 */
function factorial(n) = n == 0 ? 1 : factorial(n - 1) * n;

