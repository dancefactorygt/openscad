/**
 * Diversas funciones trigonométricas.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Math/trigonometry.scad
 * @license CC-BY-NC-4.0
 */

/**
 * Devuelve la altura y las coordenadas para dibujar una catenaria.
 *
 * @param {Float} width  Ancho de la catenaria.
 * @param {Float} factor Factor a usar para aplanar/agrandar la catenaria.
 *
 * @return {Array} 0: Altura, 1: Puntos
 */
function catenary(width, factor = 0.2) = let(_w = 0.5 * width) [
    [
        for (_x = [ 0 : 0.1 : width ]) [ _x - _w, - (cosh(factor * (_x - _w)) - cosh(factor * _w)) / factor ]
    ],
    (cosh(factor * _w) - 1) / factor
];

/**
 * Devuelve el coseno hiperbólico de un valor.
 *
 * @param {Float} x Valor de entrada.
 *
 * @return {Float}
 */
function cosh(x) = (exp(x) + exp(-x)) / 2;

/**
 * Devuelve el seno hiperbólico de un valor.
 *
 * @param {Float} x Valor de entrada.
 *
 * @return {Float}
 */
function sinh(x) = (exp(x) - exp(-x)) / 2;

/**
 * Devuelve el valor del lado de un triángulo aplicando la ley de los cosenos.
 *
 * @param {Float} sideA Lado A del triángulo.
 * @param {Float} sideB Lado B del triángulo.
 * @param {Float} angle Ángulo que forman el lado A y el lado B.
 *
 * @return {Float}
 */
function lawOfCosines(sideA, sideB, angle) = sqrt(
    pow(sideA, 2) + pow(sideB, 2) - 2 * sideA * sideB * cos(angle)
);

/**
 * Calcula el valor del lado de un triángulo a partir de 2 ángulos y
 * 1 lado usando la ley de los senos:
 *
 *   sideA    sideB
 *   -----  = -----
 *   angleA   angleB
 *
 * @param {Float} sideA  Lado A del triángulo.
 * @param {Float} angleA Ángulo opuesto al lado A.
 * @param {Float} angleB Ángulo opuesto al lado B que se quiere calcular.
 *
 * @return {Float}
 */
function saa(sideA, angleA, angleB) = sideA * sin(angleB) / sin(angleA);

/**
 * Calcula el valor del lado de un triángulo a partir de 1 ángulo y
 * 2 lados usando la ley de los senos:
 *
 *   sideA    sideB
 *   -----  = -----
 *   angleA   angleB
 *
 * @param {Float} sideA  Lado A del triángulo.
 * @param {Float} sideB  Lado B del triángulo.
 * @param {Float} angleA Ángulo opuesto al lado A.
 *
 * @return {Float}
 */
function ssa(sideA, sideB, angleA) = asin(sideB * sin(angleA) / sideA);

/**
 * Calcula el valor de un cateto a partir de la hipotenusa y el otro cateto
 * usando el teormea de Pitágoras.
 *
 * @param {Float} hyp  Valor de la hipotenusa.
 * @param {Float} side Lado del triángulo.
 *
 * @return {Float}
 */
function pythagoras(hyp, side) = sqrt(pow(hyp, 2) - pow(side, 2));
