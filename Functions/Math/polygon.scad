/**
 * Diversas funciones para trabajar con polígonos.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Math/polygon.scad
 */
//-----------------------------------------------------------------------------
/**
 * Calcula el apotema de un polígono regular dado el radio.
 *
 * @param {Float}   radius Radio del polígono.
 * @param {Integer} sides  Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function apothemFromRadius(radius, sides) = apothemFromSide(sideFromRadius(radius, sides), sides);

/**
 * Calcula el apotema de un polígono regular.
 *
 * @param {Float}   side  Longitud de cada lado.
 * @param {Integer} sides Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function apothemFromSide(side, sides) = side / (2 * tan(180 / sides));

/**
 * Calcula el apotema de un polígono regular.
 *
 * @param {Float}   radius Radio del polígono.
 * @param {Integer} sides  Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function areaFromRadius(radius, sides) = (radius * radius * sides * sin(360 / sides)) / 2;

/**
 * Calcula el apotema de un polígono regular.
 *
 * @param {Float}   side  Longitud de cada lado.
 * @param {Integer} sides Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function areaFromSide(side, sides) = apothemFromSide(side, sides) * side * sides / 2;

/**
 * Devuelve el radio de un polígono a partir de la longitud de un lado.
 *
 * @param {Float}   side  Longitud de cada lado.
 * @param {Integer} sides Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function radiusFromSide(side, sides) = side / (2 * sin(180 / sides));

/**
 * Calcula el tamaño de cada lado de un polítono regular a partir del radio.
 *
 * @param {Float}   radius Radio del polígono.
 * @param {Integer} sides  Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function sideFromRadius(radius, sides) = 2 * radius * sin(180 / sides);

/**
 * Calcula el tamaño de cada lado de un polítono regular a partir del apotema.
 *
 * @param {Float}   apothem Valor de la apotema.
 * @param {Integer} sides   Cantidad de lados del polígono.
 *
 * @return {Float}
 */
function sideFromApothem(apothem, sides) = apothem * (2 * tan(180 / sides));
