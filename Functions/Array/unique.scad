/**
 * Elimina los elementos duplicados preservando su orden.
 *
 * Ejemplo:
 *
 * ```openscad
 * in = [ 1, 2, 5, 1, 2, 1, 4, 2, 3 ];
 * echo(arrayUnique(in)); // [ 1, 2, 5, 4, 3 ];
 * ```
 *
 * @param {Float[]} array Array a modificar.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Array/unique.scad
 */
function arrayUnique(array) = [
    for (index = [ 0 : len(array) - 1 ]) if (search([ array[index] ], array, 1) == [ index ]) array[index]
];
