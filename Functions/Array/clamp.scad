/**
 * Restringe los valores del array a estar entre 2 valores límites.
 * Cualquier valor inferior será devuelto como el menor valor y cualquier valor superior
 * será devuelto como el mayor valor.
 *
 * Ejemplo:
 *
 * ```openscad
 * in = [ 1, -2, -5, -1, 2, 1, 0, 4, -2, 3 ];
 * echo(arrayClamp(in, -1.5, 1.5)); // [ 1, -1.5, -1.5, -1, 1.5, 1, 0, 1.5, -1.5, 1.5 ];
 * ```
 *
 * @param {Float[]} array Array a modificar.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Array/clamp.scad
 */
function arrayClamp(array, lower = -1, upper = 1) = is_list(array)
    ? len(array) == 0
        ? []
        : [ for (index = array) arrayClamp(index, lower, upper) ]
    : min(max(array, min(lower, upper)), max(lower, upper));
