/**
 * Funciones para aplanar arrays.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Array/flatten.scad
 * Devuelve un array de una sola dimensión a partir de un array multidimensional.
 */

/**
 * Aplana multidimensional y lo deja en una dimensión.
 *
 * Ejemplo:
 *
 * ```
 * in = [ [ 0, 1 ], [ 2, 3 ], [ [ 4 ], [ 5 ] ] ];
 * echo(arrayFlatten(in)); // [ 0, 1, 2, 3, 4, 5 ]
 * ```
 *
 * @param {Array} array Array multidimensional a usar para obtener los valores.
 *
 * @return {Array}
 */
function arrayFlatten(array) = [
    for (_i = array, _j = is_list(_i) ? arrayFlatten(_i) : _i)
        _j
];

/**
 * Aplana un array un solo nivel.
 *
 * Ejemplo:
 *
 * in = [ [ 0, 1 ], [ 2, 3 ], [ [ 4 ], [ 5 ] ];
 * echo(arrayFlatten(in)); // [ 0, 1, 2, 3, [ 4 ], [ 5 ] ]
 *
 * @param {Array} array Array multidimensional a usar para obtener los valores.
 *
 * @return {Array}
 */
function arrayFlattenOneLevel(array) = [ for (_a = array, _l = _a) _l ];
