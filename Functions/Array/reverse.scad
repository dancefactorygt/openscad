/**
 * Invierte el orden de los elementos.
 *
 * Ejemplo:
 *
 * ```openscad
 * in = [ 1, 2, 3, 4 ];
 * echo(arrayReverse(in)); // [ 4, 3, 2, 1 ];
 * ```
 *
 * @param {Float[]} array Array a modificar.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Array/reverse.scad
 */
function arrayReverse(array) = [ for (index = [ len(array) - 1 : - 1 : 0 ]) array[index] ];
