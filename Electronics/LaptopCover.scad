/**
 * Crea un tapa para colocar encima del teclado del portátil.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Electronics/LaptopCover.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <../Modules/Box/rounded.scad>
use <../Modules/Utils/flex.scad>
use <../Modules/Pattern/dovetail.scad>
//-----------------------------------------------------------------------------
corner    =   2.0;     // Radio para redondear las esquinas.
height    = 240.0;     // Alto de la tapa (eje Y).
length    =   3.0;     // Longitud del borde (eje Z).
radius    =   0.2;     // Radio para suavizar las esquinas del diente.
width     = 363.0;     // Ancho de la tapa (eje X).
thickness =   1.2;     // Grosor de la tapa (eje Z).
tolerance =   1.2;     // Tolerancia a dejar entre las partes para que puedan encajar.
twidth    =  20.0;     // Ancho de cada diente. Se recalcula para ajustarde a las medidas de la tapa y los dientes de ambos lados sean de un tamaño similar..
theight   =   5.0;     // Alto de cada diente.
sides     = [ 0 : 1 ]; // Índice de las partes a dibujar.
//-----------------------------------------------------------------------------
function getHeight() = max(width, height);
function getWidth()  = min(width, height);

blocks    = [
    [ "x", [ height / 4 - 10, 1 ] , 135 ],
    [ "x", [ 80 - height / 2, 1 ] ,  25 ],
    [ "x", [ height / 4 + 7, -1 ], 100 ],
    [ "x", [ 80 - height / 2, -1 ],  20 ]
];
module base(width = width, height = height)
{
    _l = length + thickness;
    translate([ 0, 0, _l / 2 ])
    {
        difference()
        {
            boxRounded(width, height, _l, corner);
            translate([ 0, 0, thickness / 2  ])
            {
                boxRounded(width - 2 * thickness, height - 2 * thickness, length + 0.01, corner - thickness);
            }
        }
    }
}

module blocks(config)
{
    _h = getHeight() / 2;
    _w = getWidth() / 2;
    _t = 2 * thickness;
    _l = length + _t;
    difference()
    {
        children();
        for (_c = config)
        {
            _axis   = _c[0];
            _x      = _c[1][0];
            _y      = _c[1][1];
            _length = _c[2];
            if (_axis == "x")
            {
                translate([ _x, _y * (_h - _t / 4), (_l + thickness) / 2 ])
                {
                    cube([ _length, _t, _l ], center = true);
                }
            }
            else if (_axis == "y")
            {
                translate([ _x * _w, _x, 10 * thickness ])
                {
                    cube([ _t, _length, length ]);
                }
            }
        }
    }
}

module four2()
{
    dh      = theight / 2;
    sx      = [ -1, 1,  1, -1 ];
    sy      = [  1, 1, -1, -1 ];
    th      = tolerance;
    borders = [
        [ corner,      0,      0,      0 ],
        [      0, corner,      0,      0 ],
        [      0,      0, corner,      0 ],
        [      0,      0,      0, corner ],
    ];
    // Puntos del polígono para extraer la cruz del centro.
    points = [
        [ - dh - dh       ,   dh          ],
        [ - dh            ,   dh          ],
        [             0.00,   th / 2      ],
        [   dh - th + 0.01,   dh          ],
        [   dh + th       ,   dh + 2 * th ],
        [   dh + th       , - dh          ],
        [   dh - th + 0.01, - dh          ],
        [ - dh            , - dh          ],
        [ - dh - dh       , - dh          ],
    ];
    for (n = sides)
    {
        difference()
        {
            union()
            {
                translate([ sx[n % 4] * (width  + theight) / 4, sy[n % 4] * (height + theight) / 4, 0 ])
                {
                    difference()
                    {
                        boxRounded((width - theight) / 2, (height - theight) / 2, thickness, borders[n % 4]);
                        boxRounded((width - theight) / 2 - 10, (height - theight) / 2 - 10, 2 * thickness, 0);
                    }
                }
                teeth(n);
                teeth((n + 1) % 4, true);
            }
            rotate(- n * 90)
            {
                translate([ 0, 0, - thickness ])
                {
                    linear_extrude(2 * thickness, convexity = 10)
                    {
                        polygon(points = points);
                    }
                }
            }
        }
    }
}

module twoParts()
{
    _d = 10;
    _h = getHeight();
    _w = getWidth();
    for (n = sides)
    {
        rotate(n * 180)
        {
            dovetailJoint(_w, theight, thickness, 5, tolerance, radius, "t")
            {
                difference()
                {
                    translate([ 0, 0, - thickness / 2 ])
                    {
                        base(_w, _h);
                    }
                    translate([ (_w - thickness) / 2, 0, 0 ])
                    {
                        cube([ 2 * thickness, theight, 10 * (length + thickness) ], center = true);
                    }
                    translate([ 0, _h / 4, 0 ])
                    {
                        translate([ 0, - (_h + theight) / 2 + tolerance / 4, 0 ])
                        {
                            cube([ 2 * _w, _h / 2, 10 * (length + thickness) ], center = true);
                        }
                        translate([ 0, theight / 4, 0 ])
                        {
                            // Cubo central para reducir material.
                            // cube([ _w - _d, _h / 2 - _d - theight, 2 * thickness ], center = true);
                        }
                    }
                }
            }
        }
    }
}
//-----------------------------------------------------------------------------
// Inicio del script
//-----------------------------------------------------------------------------
$fn = 100;
blocks(blocks)
{
    twoParts();
}
