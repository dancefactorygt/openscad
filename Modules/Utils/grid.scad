/**
 * Dibuja una rejilla centrada que permite medir el tamaño que ocupa una figura.
 *
 * @param {Float}  width     Ancho de la rejilla.
 * @param {Float}  height    Alto de la rejilla.
 * @param {Float}  size      Tamaño del lado de cada cuadro de la rejilla.
 * @param {Float}  thickness Grosor de las líneas.
 * @param {Float}  z         Desplazamiento en el eje Z.
 * @param {String} color     Color de las líneas.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Utils/grid.scad
 * @license CC-BY-NC-4.0
 */
module grid(width = 100, height = 100, size = 1, thickness = 0.04, z = 0, color = "Red")
{
    _ax = [ for (_x = [ - width / 2 : size : width / 2 ]) _x ];
    for (_x = [ 0 : len(_ax) - 1 ])
    {
        translate([ _ax[_x], 0, z ])
        {
            color(color, _x % 10 == 0 ? 0.8 : 0.3)
            {
                cube([ thickness, height, thickness ], center = true);
                translate([ 0, height / 2 + 1, - thickness / 2 ])
                {
                    rotate([ 0, 0, 90 ])
                    {
                        linear_extrude(thickness, convexity = 10)
                        {
                            text(str(_ax[_x]), size = 0.5, halign = "center", valign = "center");
                        }
                    }
                }
            }
        }
    }
    _ay = [ for (_y = [ - height / 2 : size : height / 2 ]) _y ];
    for (_y = [ 0 : len(_ay) - 1 ])
    {
        translate([ 0, _ay[_y], z ])
        {
            color(color, _y % 10 == 0 ? 0.8 : 0.3)
            {
                cube([ width, thickness, thickness ], center = true);
                translate([ width / 2 + 1, 0, - thickness / 2 ])
                {
                    linear_extrude(thickness, convexity = 10)
                    {
                        text(str(_ay[_y]), size = 0.5, halign = "center", valign = "center");
                    }
                }
            }
        }
    }
}
