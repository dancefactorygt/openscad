/**
 * Genera una caja para almacenar proyectos y/o objetos. También permite generar la tapa de la caja.
 * Varias cajas pueden ser colocadas de manera apilada usando una varilla roscada.
 *
 * Cada caja puede ser personalizada usando varios parámetros:
 *
 * - Medidas (ancho, alto y largo).
 * - Configuración del tornillo para apilarlas y/o colocar la tapa (diámetro del tornillo,
 *   diámetro de la cabeza, altura de la cabeza, largo del tornillo).
 * - Seleccionar las paredes a imprimir.
 * - Patrón a usar en la parte inferior para disminuir la cantidad de filamento o para permitir el paso de
 *   objetos entre cajas, por ejemplo, para conectar algunos circuitos entre sí.
 *
 * Aunque puede generar cajas, su principal uso es utilizarlo como base para otros diseños que generen
 * soportes o piezas que se colocan sobre la base.
 *
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Box/stackable.scad
 * @license CC-BY-NC-4.0
 * @see     http://www.thingiverse.com/thing:3861344
 */
border    = 1.2;               // Radio del borde de algunas esquinas.
margin    = 0;                 // Margen para el patrón de la base.
thickness = 1.2;               // Grosor de las paredes.
screw     = [ 4.2, 6.0, 2.0 ]; // Configuración del tornillo:
                               // 0 - Diámetro del tornillo.
                               // 1 - Diámetro de la cabeza del tornillo.
                               // 2 - Altura de la cabeza del tornillo.
                               // 3 - Largo del tornillo. Si no se especifica se usa el alto de la caja.
//----------------------------------------------------------
use <./rounded.scad>
use <../Pattern/bars.scad>
use <../Pattern/honeycomb.scad>
//----------------------------------------------------------
// Constantes
//----------------------------------------------------------
SCREW_DIAMETER      = 0; // Diámetro del tornillo.
SCREW_HEAD_DIAMETER = 1; // Diámetro de la cabeza del tornillo.
SCREW_HEAD_HEIGHT   = 2; // Altura de la cabeza del tornillo.
SCREW_LENGTH        = 3; // Longitud del tornillo.
//----------------------------------------------------------
// Calcula el tamaño de cada lado de las esquinas en función del tornillo y el groso de las paredes.
function getSide(screw, thickness) = let(_hd = screw[SCREW_HEAD_DIAMETER], _sd = screw[SCREW_DIAMETER])
    max(_hd ? _hd : 0, _sd ? _sd : 0) + 2 * thickness;
//----------------------------------------------------------

/**
 * Dibuja cada una de las esquinas.
 *
 * @param {Float}   height    Altura de la esquina.
 * @param {Float[]} screw     Configuración del tornillo.
 * @param {Float}   thickness Grosor de las paredes.
 * @param {Float}   border    Radio de los bordes.
 */
module boxStackableCorner(height, screw = screw, thickness = thickness, border = border)
{
    _s = getSide(screw, thickness);
    difference()
    {
        boxRounded(_s, _s, height, border);
        _eps = 0.01;
        _sd  = screw[SCREW_DIAMETER];
        assert(is_num(_sd), "El diámetro del tornillo debe ser un número");
        if (is_num(_sd) && _sd > 0)
        {
            _sl = screw[SCREW_LENGTH];
            _l  = _sl == undef ? height : _sl;
            assert(is_num(_l), "La longitud del tornillo debe ser un número");
            translate([ 0, 0, - (_l + _eps) / 2 ])
            {
                cylinder(d = _sd, h = height + _eps);
            }
            _hd = screw[SCREW_HEAD_DIAMETER];
            if (_hd != undef)
            {
                assert(_hd == 0 || _hd > _sd, "El diámetro de la cabeza no puede ser menor que el del tornillo");
                assert(is_num(_hd), "El diámetro de la cabeza del tornillo debe ser un número");
                _hh = screw[SCREW_HEAD_HEIGHT];
                assert(is_num(_hh), "La altura de la cabeza del tornillo debe ser un número");
                assert(border < _s / 2, "El radio del borde no puede ser mayor que el lado de la esquina");
                if (_hh > 0)
                {
                    for (_angle = [ 0, 180 ])
                    {
                        rotate([ _angle, 0, 0 ])
                        {
                            translate([ 0, 0, height / 2 - _hh ])
                            {
                                cylinder(d = _hd, h = _hh + _eps);
                                translate([ 0, 0, - _hd / 2 + _eps ])
                                {
                                    cylinder(d1 = 0, d2 = _hd, h  = _hd / 2);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/**
 * Dibuja la tapa de la bandeja.
 *
 * @param {Float}   width     Ancho de la tapa.
 * @param {Float}   length    Longitud de la tapa.
 * @param {Float[]} screw     Configuración del tornillo.
 * @param {Float}   thickness Grosor de la tapa.
 * @param {Float}   border    Radio de los bordes.
 */
module boxStackableCover(width, length, screw = screw, thickness = thickness, border = border)
{
    _d = screw[SCREW_DIAMETER];     // Diámetro del tornillo.
    _a = max([ _d, screw[SCREW_HEAD_DIAMETER] ]);
    _l = thickness + _a / 2;
    difference()
    {
        _eps = 0.01;
        boxRounded(width, length, thickness, border);
        for (_x = [ -1, 1 ])
        {
            for (_y = [ -1, 1 ])
            {
                translate([ _x * (width / 2 - _l), _y * (length / 2 - _l), 0 ])
                {
                    cylinder(d = _d, h = thickness + _eps, center = true);
                }
            }
        }
    }
}

/**
 * Dibuja un patrón sobre la base para minimizar la superficie a imprimir y permitiendo
 * el paso de objetos entre bandejas.
 *
 * @param {Float}         width     Ancho de la parte interna de la bandeja (eje X)
 * @param {Float}         height    Altura de la bandeja (eje Y)
 * @param {Float}         length    Largo de la bandeja (eje Z)
 * @param {Float}         thickness Grosor de las paredes.
 * @param {String}        pattern   Patrón a usar para minimizar el uso de filamento en la base.
 * @param {Float|Float[]} margin    Margen a dejar al dibujar el patrón en la base.
 * @param {Float}         radius    Radio a usar en algunos patrones.
 * @param {Float}         cellSize  Tamaño de la celda. Si no se especifica se intenta calcular un valor.
 */
module boxStackablePattern(width, height, length, thickness = thickness, pattern = "none", margin = 0, radius = 1, cellSize = -1)
{
    _e = $preview ? 0.01 : 0;
    _m = is_list(margin) ? margin : [ margin, margin, margin, margin ];
    _t = 2 * thickness;
    _h = height - _m[0] - _m[2];
    _w = width - _m[1] - _m[3];
    translate([ width / 2, height / 2, length / 2 ])
    {
        if (pattern == "bars" || pattern == "rounded" || pattern == "rtriangle" || pattern == "triangle")
        {
            _bw = 6 * thickness;
            _l  = length + 2 * _e;
            translate([ 0, 0, - _l / 2 ])
            {
                linear_extrude(_l, convexity = 10)
                {
                    bars(_w - _bw + _e, _bw, _h + _e, _w / (1.5 * _bw), pattern);
                }
            }
        }
        else if (pattern == "honeycomb")
        {
            _cell = cellSize > 0
                ? cellSize
                : width >= height
                    ? height / 3
                    : height / 5;
            honeycombCube(_w + _e, _h + _e, length + _e, _cell, thickness);
        }
        else if (pattern == "rect" && (_w != width || _h != height))
        {
            cube([ _w, _h, length + _e ], center = true);
        }
    }
}

/**
 * Módulo para dibuja una bandeja de manera genérica.
 *
 * @param {Float}         width     Ancho de la parte interna de la bandeja (eje X)
 * @param {Float}         height    Altura de la bandeja (eje Y).
 * @param {Float}         length    Largo de la bandeja (eje Z).
 * @param {Float[]}       screw     Configuración del tornillo.
 * @param {Float}         thickness Grosor de las paredes
 * @param {Float}         border    Radio del borde de las esquinas.
 * @param {Float}         base      Longitud de la base.
 *                                  Si no se especifica se usa la longitud de la bandeja.
 * @param {Float[]}       walls     Índices de las paredes a dibujar.
 * @param {String}        pattern   Patrón a usar para minimizar el uso de filamento en la base.
 * @param {Float|Float[]} pmargin   Margen a dejar al dibujar el patrón en la base.
 * @param {Float}         pradius   Radio a usar en algunos patrones.
 */
module boxStackableTray(width, height, length, screw = screw, thickness = thickness, border = border, base = 0, walls = [ 1, 2, 3, 4 ], pattern = "none", pmargin = 0, pradius = 1)
{
    _s = getSide(screw, thickness);
    _l = length - 2 * _s;
    _b = base && base < length ? base - _s : _l;
    _w = width - 2 * _s;
    //-----------------------------------------------------------------
    // Esquinas
    //-----------------------------------------------------------------
    for (_x = [ -1, 1 ])
    {
        for (_y = [ -1, 1 ])
        {
            translate([ _x * (width - _s) / 2, _y * (length - _s) / 2, 0])
            {
                boxStackableCorner(height, screw, thickness, border);
            }
        }
    }
    //-----------------------------------------------------------------
    // Pletinas que unen las esquinas
    //-----------------------------------------------------------------
    for (_x = [ -1, 1 ])
    {
        translate([ _x * (_w + _s) / 2, 0, - (height - thickness) / 2 ])
        {
            cube([ _s, _l + 2 * thickness, thickness ], center = true);
        }
    }
    for (_y = [ -1, 1 ])
    {
        translate([ 0, _y * (_l + _s) / 2, - (height - thickness) / 2 ])
        {
            cube([ _w + 2 * thickness, _s, thickness ], center = true);
        }
    }
    //-----------------------------------------------------------------
    // Paredes de la bandeja.
    //-----------------------------------------------------------------
    for (_x = [ -1, 1 ])
    {
        if (search(_x == -1 ? 1 : 3, walls))
        {
            translate([ _x * (width - thickness) / 2, 0, 0 ])
            {
                cube([ thickness, length - _s, height ], center = true);
            }
        }
    }
    for (_y = [ -1, 1 ])
    {
        if (search(_y == -1 ? 4 : 2, walls))
        {
            translate([ 0, _y * (length - thickness) / 2, 0 ])
            {
                cube([ width - _s, thickness, height ], center = true);
            }
        }
    }
    //-----------------------------------------------------------------
    // Base
    //-----------------------------------------------------------------
    translate([ - _w / 2, - length / 2 + _s, - height / 2 ])
    {
        difference()
        {
            cube([ _w, _b, thickness ]);
            boxStackablePattern(_w, _b, thickness, 2 * thickness, pattern, pmargin, pradius);
        }
    }
}
