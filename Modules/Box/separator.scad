/**
 * Módulo para generar un separador rectangular con una muesca para ahorrar filamento en el centro.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Box/separator.scad
 * @license CC-BY-NC-4.0
 *
 * @param {Float} width     Ancho del espacio a dejar en el separador.
 * @param {Float} length    Longitud del espacio a dejar en el separador.
 * @param {Float} thickness Grosor del separador.
 * @param {Float} radius    Radio a usar para redondear el borde.
 */
module boxSeparator(width, length, thickness = 1.2, radius = 4, negative = true)
{
    _fn = $fn ? $fn : 10;
    _r  = radius;
    _r2 = _r * _r;
    _w  = width / 2;
    _a  = _w;
    _b  =  length - _r;
    _p  = [
        [  0,  0      ],
        // Inferior
        for (_x = [ 0 : _r / _fn : _r ]) [ _a - 2 * _r + _x, - sqrt(_r2 - pow(_x, 2)) + _r ],
        // Superior
        for (_x = [ - _r : _r / _fn : 0 ]) [ _a + _x, sqrt(_r2 - pow(_x, 2)) + _b ],
        [ _w, _b + _r ],
        [ _w, length + _r ],
        [  0, length + _r ]
    ];
    module _()
    {
        union()
        {
            polygon(points = _p);
            mirror([ 1, 0, 0 ])
            {
                polygon(points = _p);
            }
        }
    }

    linear_extrude(thickness, convexity = 10)
    {
        if (negative)
        {
            _();
        }
        else
        {
            difference()
            {
                translate([ - width / 2, 0 ])
                {
                    square([ width, length ]);
                }
                _();
            }
        }
    }
}
