/**
 * Módulos para repartir de manera concéntrica algunas formas geométricas.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/concentric.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <../../Functions/Array/sum.scad>
//-----------------------------------------------------------------------------
/**
 * Calcula los ángulos en los que hay que colocar cada elemento de manera concéntrica.
 *
 * @param {Float[]} sizes  Tamaños a repartir.
 * @param {Float}   radius Radio sobre el que se dibujará el patrón concéntrico.
 */
function getConcentricAngles(sizes, radius) = let(
        _count  = len(sizes),
        _sum    = arraySum(sizes, _count),
        _length = 2 * PI * radius,
        _margin = (_length - _sum) / _count
    ) [
        for (_i = [ 0 : _count - 1 ]) (180 / PI) * (arraySum(sizes, _i) + _i * _margin) / radius
    ];

/**
 * Calcula el radio mínimo para colocar los elementos de manera concéntrica.
 *
 * @param {Float[]} sizes     Tamaños a repartir.
 * @param {Float}   margin    Margen de separación entre elementos.
 * @param {Float}   tolerance Tolerancia a usar en el radio a devolver.
 */
function getConcentricRadius(sizes, margin, tolerance = 0) = let(_len = len(sizes))
    (arraySum(sizes, _len) + margin * _len) / (2 * PI) + tolerance;

/**
 * Repite una forma 2D generando un patrón concéntrico centrado.
 *
 * @param {Float} diameter  Diámetro del patrón a generar (eje radial).
 * @param {Float} width     Ancho de cada elemento (eje circular).
 * @param {Float} height    Altura de cada elemento (eje radial).
 * @param {Float} margin    Margen de separación entre elementos.
 * @param {Float} from      Valor inicial del radio donde se va a empezar (eje radial).
 */
module concentric(diameter, width, height, margin, from = 0)
{
    _dc = width  + margin;
    _dr = height + margin;
    for (_x = [ from : _dr : (diameter - _dr) / 2 ])
    {
        _l = 2 * PI * _x;
        _a = 360 / floor(_l / _dc);
        concentricAngles([ 0 : _a : 360 ], _x, true)
        {
            children();
        }
    }
}

/**
 * Coloca un elemento según los ángulos especificados.
 *
 * @param {Float[]} angles Ángulos de colocación.
 * @param {Float}   radius Radio de rotación.
 */
module concentricAngles(angles, radius = 0, rotate = false)
{
    for (_angle = angles)
    {
        rotate(_angle)
        {
            translate([ radius, 0 ])
            {
                if (rotate)
                {
                    rotate(- _angle)
                    {
                        children();
                    }
                }
                else
                {
                    children();
                }
            }
        }
    }
}


/**
 * Dado un conjunto de diámetros, los reparte de manera concéntrica.
 *
 * @param {Float[]} diameters Diámetros a repartir (eje radial).
 * @param {Float}   margin    Margen a dejar entre elementos.
 * @param {Float[]} heights   Altura de los cilindros (eje Z).
 *                            Si tienen valores negativos, se alinean en la parte superior.
 *                            Si no se especifica se dibujan círculos.
 * @param {Float}   offset    Permite ajustar la posición de los elementos para alinearlos.
 * @param {Float}   tolerance Tolerancia a usar en los diámetros.
 */
module concentricCircles(diameters, margin, heights = [], center = true, offset = 0, tolerance = 0)
{
    _len    = len(diameters);
    _radius = _len > 1 ? getConcentricRadius(diameters, margin)  : 0;
    _angles = _len > 1 ? getConcentricAngles(diameters, _radius) : [0];
    _dmax   = max(diameters);
    for (_index = [ 0 : _len - 1 ])
    {
        _d = diameters[_index] + tolerance;
        _a = center
            ? _angles[_index]
            : _angles[_index] - (180 / PI) * (_d + margin) / (2  * _radius);
        rotate(_a)
        {
            translate([ _radius + offset * (_dmax - _d) / 2, 0 ])
            {
                _h = heights[_index];
                if (is_num(_h) && _h != 0)
                {
                    if (_h < 0)
                    {
                        translate([ 0, 0, - min(heights) + _h ])
                        {
                            cylinder(d = _d, h = - _h);
                        }
                    }
                    else
                    {
                        cylinder(d = _d, h = _h);
                    }
                }
                else
                {
                    circle(d = _d);
                }
            }
        }
    }
}
