/**
 * Repite una forma 2D generando un patrón rectangular centrado.
 *
 * @param {Float}         side      Longitud del lado a llenar con patrones.
 * @param {Float}         width     Ancho de cada elemento (eje X).
 * @param {Float}         height    Altura de cada elemento (eje Y).
 * @param {Float|Float[]} thickness Grosor de las paredes del patrón.
 * @param {Float}         sideY     Permite especificar la otra dimensión cuando el área es rectangular.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/rectangular.scad
 * @license CC-BY-NC-4.0
 */
module rectangular(side, width, height, thickness, sideY = 0)
{
    _tx     = is_list(thickness) ? thickness[0] : thickness;
    _ty     = is_list(thickness) ? thickness[1] : thickness;
    _h      = height + _ty;
    _w      = width  + _tx;
    _sx     = side;
    _sy     = sideY ? sideY : side;
    _countX = ceil(_sx / _w);
    _countY = ceil(_sy / _h);
    translate([ - (_countX * _w - _w) / 2, - (_countY * _h - _h) / 2 ])
    {
        for (_x = [ 0 : _countX - 1 ])
        {
            for (_y = [ 0 : _countY - 1 ])
            {
                translate([ _x * _w, _y * _h ])
                {
                    children();
                }
            }
        }
    }
}
