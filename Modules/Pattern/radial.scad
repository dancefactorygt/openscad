/**
 * Distribuye un grupo de elementos en forma radial de tal manera que
 * ocupen todo el espacio posible sobre una circunferencia.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/radial.scad
 * @license CC-BY-NC-4.0
 *
 * @param {Integer}   count Cantidad de elementos a distribuir.
 * @param {Float}     width Ancho de cada elemento.
 * @param {Integer[]} skip  Índice de los elementos a omitir.
 */
module radial(count, width, skip = [])
{
    if (count > 0)
    {
        _r = count == 1
            ? 0
            : width / (2 * sin(180 / count));
        for (_n = [ 0 : count - 1 ])
        {
            if (!search(_n, skip))
            {
                rotate(_n * 360 / count)
                {
                    translate([ _r, 0 ])
                    {
                        children();
                    }
                }
            }
        }
    }
}
