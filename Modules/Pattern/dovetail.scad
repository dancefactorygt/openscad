/**
 * Módulos para generar el patrón de cola de milano para el ensamble de piezas.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/dovetail.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <../Utils/flex.scad>
//-----------------------------------------------------------------------------
/**
 * Devuelve el ancho que debe ocupar el diente para cubrir el ancho con la cantidad
 * de dientes especificados..
 *
 * @param {Float}   width Ancho total
 * @param {Integer} count Cantidad de dientes a dibujar en el ancho especificado.
 *
 * @return {Float} Ancho de cada diente.
 */
function getDovetailWidth(width, count) = count > 0
    ? width / (1.5 * count - 0.5)
    : width;

/**
 * Dibuja una tira de dientes de la cola de milano.
 * El ancho del diente se calcula en función del ancho a cubrir y la cantidad.
 *
 * @param {Float}   width     Ancho de la superficie donde se colocará la tira de dientes (eje X).
 * @param {Float}   height    Alto del diente (eje Y).
 * @param {Float}   thickness Grosor del diente.
 * @param {Integer} count     Cantidad de dientes a colocar.
 * @param {Float}   tolerance Tolerancia a usar para dejar separación entre los dientes.
 * @param {Float}   radius    Radio a usar para redondear la esquina.
 * @param {String}  ends      Extremos en los que dibujar medio diente (l | r).
 */
module dovetail(width, height, thickness = 2, count = 5, tolerance = 0.5, radius = 0.001, ends = "")
{
    _w = getDovetailWidth(width, count);
    _t = _w - 2 * tolerance;
    flexCenter(width - _t, count)
    {
        dovetailTooth(_t, height, thickness, radius);
    }
    for (_end = ends)
    {
        _m = _end == "l" ? [ 0, 1, 0 ]
           : _end == "r" ? [ 1, 0, 0 ]
           : false;
        assert(_m, "El parámetro `ends` solamente puede tener como valores 'l' y/o 'r'");
        if (_m)
        {
            translate([ (_end == "l" ? -1 : 1) * width / 2, 0, 0 ])
            {
                mirror(_m)
                {
                    dovetailHalfTeeth(_w, height, thickness);
                }
            }
        }
    }
}

/**
 * Realiza la unión con cola de milano del modelo pasado como hijo.
 *
 * @param {Float}   width     Ancho de la superficie donde se colocará la cola de milano (eje X).
 * @param {Float}   height    Alto del diente (eje Y).
 * @param {Float}   thickness Grosor del diente.
 * @param {Integer} count     Cantidad de dientes a colocar.
 * @param {Float}   tolerance Tolerancia a usar para dejar separación entre los dientes.
 * @param {Float}   radius    Radio a usar para redondear la esquina.
 * @param {String}  sides     Lados de la cola de milano a dibujar ([ b | t ]).
 */
module dovetailJoint(width, height, thickness = 2, count = 5, tolerance = 0.5, radius = 0.001, sides = "bt")
{
    _w = getDovetailWidth(width, count - 1.25);
    difference()
    {
        children();
        cube([ width, height, thickness ], center = true);
    }
    for (_s = [ "b", "t" ])
    {
        if (search(_s, sides))
        {
            _n = _s == "b" ? 1 : -1;
            _r = _s == "b" ? 0 : 180;
            translate([ _n * _w / 4, - _n * tolerance / 4, - thickness / 2 ])
            {
                rotate(_r * 180)
                {
                    dovetail(width - _w / 2 , height, thickness, count, tolerance, radius, [ "l" ]);
                }
            }
        }
    }
}

/**
 * Dibuja la mitad de diente de la cola de milano para colocarlo al principio/final.
 *
 * @param {Float} width     Ancho del diente
 * @param {Float} height    Altura del diente.
 * @param {Float} thickness Grosor del diente.
 */
module dovetailHalfTeeth(width, height, thickness)
{
    _h = height / 2;
    _w = width  / 2;
    linear_extrude(height = thickness, convexity = 10)
    {
        polygon(
            [
                [        0, - _h ],
                [        0,   _h ],
                [   _w / 2,   _h ],
                [   _w    , - _h ]
            ]
        );
    }
}

/**
 * Dibuja una tira de dientes de la cola de milano.
 *
 * @param {Float}    width     Ancho de la superficie donde se colocará la tira de dientes (eje X).
 * @param {Float}    height    Alto de la superficie donde se colocará la tira de dientes (eje Y).
 * @param {Float}    thickness Grosor del diente.
 * @param {Float}    tolerance Tolerancia a usar para dejar separación entre los dientes.
 * @param {String[]} ends      Extremos a dibujar medio diente ([ l | r ]).
 * @param {Float[]}  tooth     Tamaño del diente ([ ancho, alto ])
 */
module dovetailTeeth(width, height, thickness, tolerance = 0, ends = [], tooth = [ 20, 5 ])
{
    l = width;
    c = ceil(l / (2 * tooth[0]));
    w = getDovetailWidth(l, c);
    d = tooth[1] / 2;
    translate([ l / 2, 0, - thickness / 2 ])
    {
        dovetail(
            w,
            tooth[1] - tolerance / 2,
            thickness + 0.02,
            ends ? c : c - 1,
            tolerance,
            ends
        );
    }
}

/**
 * Dibuja un diente de la cola de milano.
 *
 * @param {Float} width     Ancho del diente
 * @param {Float} height    Altura del diente.
 * @param {Float} thickness Grosor del diente.
 * @param {Float} radius    Radio a usar para redondear las esquinas.
 */
module dovetailTooth(width, height, thickness, radius = 0.001)
{
    _h = height / 2 - radius;
    _w = width  / 2 - 2 * radius;
    _p = [
        [ - _w    , - _h ],
        [ - _w / 2,   _h ],
        [   _w / 2,   _h ],
        [   _w    , - _h ]
    ];
    linear_extrude(height = thickness, convexity = 10)
    {
        hull()
        {
            for (_a = _p)
            {
                translate(_a)
                {
                    circle(r = radius > 0 ? radius : 0.001);
                }
            }
        }
    }
}
