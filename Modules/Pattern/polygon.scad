/**
 * Dibuja polígono regulares con las esquinas redondeadas.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/polygon.scad
 * @license CC-BY-NC-4.0
 */

/**
 * Dibuja un polígono regular 2D con las esquinas redondeadas.
 *
 * @param {Float}   radius Radio del polígono.
 * @param {Integer} sides  Cantidad de lados.
 * @param {Float}   corner Radio de cada esquina.
 */
module polygon2d(radius, sides, corner = 1)
{
    hull()
    {
        for (_angle = [ 0 : 360 / sides : 359 ])
        {
            rotate(_angle)
            {
                translate([ radius - corner, 0 ])
                {
                    circle(r = corner);
                }
            }
        }
    }
}

/**
 * Dibuja un polígono regular 3D con las esquinas redondeadas.
 *
 * @param {Float}   height Altura del polígono.
 * @param {Float}   radius Radio del polígono.
 * @param {Integer} sides  Cantidad de lados.
 * @param {Float}   corner Radio de cada esquina.
 */
module polygon3d(height, radius, sides, corner = 1)
{
    linear_extrude(height, convexity = 10)
    {
        polygon2d(radius, sides, corner);
    }
}