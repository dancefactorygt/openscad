/**
 * Genera un contenedor para almacenar pilas.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/children/batteries.scad
 * @license CC-BY-NC-4.0
 */
//---------------------------------------------------------------
use <../battery.scad>
use <./common.scad>
//---------------------------------------------------------------

/**
 * Soporte para pilas cilíndricas.
 *
 * @param {Float}  diameter  Diámetro del cilindro.
 * @param {Float}  margin    Margen a dejar desde el borde superior de la batería hasta el cilindro.
 * @param {Float}  thickness Grosor de las paredes entre las pilas.
 * @param {String} model     Nombre del modelo de la pila.
 * @param {Float}  tolerance Tolerancia a usar dependiendo de la punta de la impresora.
 * @param {Float}  from      Radio inicial para empezar (eje radial).
 * @param {String} name      Nombre del modelo a generar.
 *
 * @see https://en.wikipedia.org/wiki/List_of_battery_sizes
 */
module cylinderBatteries(diameter, thickness, margin = 0, model = "AAA", tolerance = 0.6, from = 0, name = "cylinder-batteries")
{
    _battery = getBattery(model);
    assert(_battery, str("Tipo de pila desconocido: ", model));
    cylinderShapeConcentric(
        diameter  = diameter,
        length    = _battery[2] - margin,
        width     = _battery[1] + tolerance,
        height    = _battery[1] + tolerance,
        thickness = thickness,
        from      = from,
        name      = str(name, "-", model)
    );
}
