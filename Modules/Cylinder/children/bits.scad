/**
 * Genera soportes para almacenar puntas de taladro hexagonales.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/children/bits.scad
 * @license CC-BY-NC-4.0
 */
//---------------------------------------------------------------
use <./common.scad>
//---------------------------------------------------------------
/**
 * Dibuja los soportes para puntas de taladro.
 *
 * @param {Float}  diameter  Diámetro del cilindro.
 * @param {Float}  length    Longitud del cilindro (eje Z).
 * @param {Float}  thickness Grosor de las paredes del patrón.
 * @param {Float}  size      Tamaño en pulgadas del diámetro de la punta.
 * @param {String} type      Tipo de módulo a usar como soporte (`conc` o `rect`).
 * @param {Float}  tolerance Tolerancia a usar para dejar holgura según la impresora.
 * @param {String} name      Nombre del modelo a generar.
 */
module cylinderBits(diameter, length = 1, thickness = 0.6, size = 1/4, type = "rect", tolerance = 0.25, name = "cylinder-bits")
{
    _mm = 25.4;
    _d  = size * _mm;
    _h  = tolerance + _d;
    _w  = tolerance + _d / cos(30);
    echo(str("Bits: ", _h, "x", _w));
    if (type == "conc")
    {
        _m  = max(_h, _w);
        cylinderShapeConcentric(diameter, length, thickness, _m, _m, name = name)
        {
            circle(d = _m, $fn = 6);
        }
    }
    else if (type == "rect")
    {
        cylinderShapeRectangular(diameter, length, thickness, _w, _h, name = name);
    }
}
