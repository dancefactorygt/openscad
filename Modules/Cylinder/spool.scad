/**
 * Módulo para dibujar carretes.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/spool.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <./hollow.scad>
use <../Shapes/propeller.scad>
//-----------------------------------------------------------------------------
/**
 * Dibuja un clindro que puede ser usado para enrollar papel, filamento, etc.
 * Se le puede poner un agujero central para colocarle un tornillo.
 *
 * @param {Float}   diameter  Diámetro del cilindo.
 * @param {Float}   height    Altura del cilindro.
 * @param {Float}   thickness Grosor de las paredes del cilindro.
 * @param {Float}   rod       Diámetro del tornillo central.
 * @param {Float[]} angles    Listado de ángulos de los soportes.
 * @param {Float}   border    Ancho de la pestaña a usar en el borde.
 */
module spool(diameter, height, thickness, rod, angles = [], border = 0)
{
    _e = is_undef(epsilon) ? ($preview ? 0.001 : 0) : epsilon;
    difference()
    {
        union()
        {
            cylinderHollow(diameter, height, thickness);
            if (angles)
            {
                propeller(diameter - thickness / 2, height, thickness, angles);
            }
        }
        if (rod)
        {
            translate([ 0, 0, height / 2 - _e / 2 ])
            {
                cylinder(d = rod, h = height + 2 * _e, center = true);
            }
        }
    }
    if (rod)
    {
        cylinderHollow(rod + thickness, height, thickness);
    }
    if (border)
    {
        cylinderHollow(diameter + 2 * border, thickness, border);
    }
}
