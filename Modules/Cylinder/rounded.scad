/**
 * Dibuja un clindro con los bordes redondeados.
 *
 * @param {Float}   diameter Diámetro del cilindro.
 * @param {Float}   height   Altura del cilindro.
 * @param {Float}   top      Radio a usar para redondear el borde superior.
 * @param {Float}   bottom   Radio a usar para redondear el borde inferior.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/rounded.scad
 * @license CC-BY-NC-4.0
 */
module cylinderRounded(diameter, height, top = 1, bottom = 1)
{
    if (diameter > 0 && height > 0)
    {
        _r = diameter / 2;
        rotate_extrude(convexity = 10)
        {
            difference()
            {
                square([ _r, height ]);
                for (_y = [ -1, 1 ])
                {
                    _b = _y == -1 ? bottom : top;
                    if (_b > 0)
                    {
                        translate([ _r - _b, (1 + _y) * height / 2 ])
                        {
                            difference()
                            {
                                translate([ _b / 2, - _y * _b / 2 ])
                                {
                                    square(_b, center = true);
                                }
                                translate([ 0, - _y * _b ])
                                {
                                    circle(r = _b);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
