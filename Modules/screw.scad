/**
 * Módulo para generar modelos de tornillos para realizar las diferencias.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/screw.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
module screwCheckDiameters(screwDiameter, headDiameter)
{
    assert(
        screwDiameter <= headDiameter,
        "El diámetro de la cabeza del tornillo no puede ser menor que el diámetro del tornillo"
    );
}
//-----------------------------------------------------------------------------
/**
 * Dibuja un tornillo con cabeza cilíndrica.
 *
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, altura ]).
 * @param {Integer} sides    Cantidad de lados de la cabeza del tornillo.
 */
module screwCylHead(height, diameter, head = [], sides = undef)
{
    cylinder(d = diameter, h = height, center = true);
    if (is_list(head))
    {
        _h0 = is_undef(head[0]) ? diameter + 2 : head[0];
        _h1 = is_undef(head[1]) ? 1            : head[1];
        screwCheckDiameters(diameter, _h0);
        translate([ 0, 0, (height - _h1) / 2 ])
        {
            cylinder(d = _h0, h = _h1, center = true, $fn = sides ? sides : $fn);
        }
    }
}

/**
 * Dibuja un tornillo con cabeza plana.
 *
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, ?ángulo, ?offset ]).
 * @param {Float}   offset   Agrega un pequeño cilindo para embutir toda la cabeza.
 */
module screwFlatHead(height, diameter, head = [], offset = 0.1)
{
    cylinder(d = diameter, h = height, center = true);
    if (is_list(head))
    {
        _h0 = is_undef(head[0]) ? diameter * 2 : head[0];
        _h1 = is_undef(head[1]) ? 90           : head[1];
        screwCheckDiameters(diameter, _h0);
        _r  = _h0 / 2;           // Radio de la cabeza.
        _a  = _h1 / 2;           // Ángulo medio.
        _h  = _r * tan(90 - _a); // Altura de la cabeza
        translate([ 0, 0, height / 2 - _h ])
        {
            rotate_extrude(convexity = 10)
            {
                polygon(
                    points = [
                        [  0, 0  ],
                        [  0, _h ],
                        [ _r, _h ]
                    ]
                );
            }
        }
        _o = is_undef(head[2]) ? offset : head[2];
        if (_o > 0)
        {
            translate([ 0, 0, height / 2 - 0.001 ])
            {
                cylinder(r = _r, h = _o);
            }
        }
    }
}
/**
 * Dibuja un tornillo con cabeza hexagonal.
 *
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, altura ]).
 */
module screwHexHead(height, diameter, head = [])
{
    screwCylHead(height, diameter, head, 6);
}


/**
 * Dibuja los agujeros de los tornillos.
 *
 * @param {Float}   dx       Separación entre tornillos (eje X).
 * @param {Float}   dy       Separación entre tornillos (eje Y).
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, ángulo ]).
 * @param {String}  type     Tipo de tornillo.
 */
module screwHoles(points, height, diameter, head = [], type = "")
{
    for (_p = points)
    {
        translate(_p)
        {
            if (type == "cyl")
            {
                screwCylHead(height, diameter, head);
            }
            else if (type == "flat")
            {
                screwFlatHead(height, diameter, head);
            }
            else if (type == "hex")
            {
                screwHexHead(height, diameter, head);
            }
            else if ($children)
            {
                children();
            }
            else
            {
                cylinder(d = diameter, h = height, center = true);
            }
        }
    }
}

/**
 * Dibuja los agujeros de los tornillos.
 *
 * @param {Float}   dx       Separación entre tornillos (eje X).
 * @param {Float}   dy       Separación entre tornillos (eje Y).
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, ángulo ]).
 * @param {String}  type     Tipo de tornillo.
 */
module screwHolesXY(dx, dy, height, diameter, head = [], type = "")
{
    _p = [
        for (_x = [ -1, 1 ], _y = [ -1, 1 ])
            [ _x * dx / 2, _y * dy / 2, 0 ]
    ];
    screwHoles(_p, height, diameter, head, type);
}
