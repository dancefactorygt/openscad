/**
 * Aplica una seria de operaciones de manera recursiva.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/builder.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
colors = [
    [ "circle",   "Teal"       ],
    [ "cube",     "DarkOrange" ],
    [ "cylinder", "CadetBlue"  ],
    [ "oval",     "LightSeaGreen"  ],
    [ "square",   "OrangeRed"  ],
    [ "sphere",   "SteelBlue"  ]
];
//-----------------------------------------------------------------------------
function buildConfig(name, params, translate = [ 0, 0, 0 ], rotate = [ 0, 0, 0 ], children = [], colors = []) = [
    name,
    params,
    is_list(translate) ? translate : [ 0, 0, 0 ],
    is_list(rotate)    ? rotate    : [ 0, 0, 0 ],
    is_list(children)  ? children  : [],
    is_list(colors)    ? colors    : []
];

/**
 * Devuelve el color a usar para la figura especificada.
 *
 * @param {String} name Nombre de la figura.
 *
 * @return {Float[]|String|undef}
 */
function getShapeColor(shape) = let(_index = search([ shape ], colors, 1, 0))
    _index ? colors[_index[0]][1] : undef;

//-----------------------------------------------------------------------------
module builder(config, thickness, colors = colors)
{
    for(_i = config)
    {
        //echo(_i);
        _c = buildConfig(_i[0], _i[1], _i[2], _i[3], _i[4]);
        color(getShapeColor(_c[0]))
        {
            translate(_c[2])
            {
                rotate(_c[3])
                {
                    difference()
                    {
                        if (_c[0] == "circle")
                        {
                            linear_extrude(_c[1][1] ? _c[1][1] : thickness)
                            {
                                circle(d = _c[1][0]);
                            }
                        }
                        else if (_c[0] == "cube")
                        {
                            cube(_c[1], center = true);
                        }
                        else if (_c[0] == "cylinder")
                        {
                            cylinder(d = _c[1][0], h = _c[1][1], center = _c[1][2] != false);
                        }
                        else if (_c[0] == "oval")
                        {
                            _d = _c[1][0];
                            _l = _c[1][1] - _d;
                            linear_extrude(_c[1][2] ? _c[1][2] : thickness)
                            {
                                if (_l > 0)
                                {
                                    hull()
                                    {
                                        for (_x = [ -1, 1 ])
                                        {
                                            translate([ _x * _l / 2, 0 ])
                                            {
                                                circle(d = _d);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    circle(d = _d);
                                }
                            }
                        }
                        else if (_c[0] == "square")
                        {
                            linear_extrude(_c[1][2] ? _c[1][2] : thickness)
                            {
                                square([ _c[1][0], _c[1][1] ], center = true);
                            }
                        }
                        else if (_c[0] == "sphere")
                        {
                            sphere(d = _c[1]);
                        }
                        if (_c[4] && is_list(_c[4]))
                        {
                            builder(_c[4]);
                        }
                    }
                }
            }
        }
    }
}
