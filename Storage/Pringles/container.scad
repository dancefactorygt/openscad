/**
 * Genera un contenedor para usarse con las tapas de las latas Pringles.
 *
 * En caso de no disponer de la tapa, se puede usar el módulo `lid` para generarla.
 *
 * También aporta diversos módulos para crear patrones rectangulares o concéntricos
 * a partir de figuras 2D o divisiones radiales.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Storage/Pringles/container.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:3612377
 */
//---------------------------------------------------------------
use <../../Functions/Math/line.scad>
use <../../Modules/Box/rounded.scad>
use <../../Modules/Cylinder/battery.scad>
use <../../Modules/Cylinder/children/batteries.scad>
use <../../Modules/Cylinder/children/bits.scad>
use <../../Modules/Cylinder/children/common.scad>
use <../../Modules/Cylinder/children/memory.scad>
use <../../Modules/Cylinder/children/segments.scad>
use <../../Modules/Memory/micro-sd.scad>
use <../../Modules/Memory/sd.scad>
//---------------------------------------------------------------
$fn        = $preview ? 90 : 240;
INNER      = 73;   // Diámetro interior del cilindro.
OUTER      = 77.7; // Diámetro exterior del cilindro.
LENGTH     = 35;   // Altura por defecto del cilindro. En algunos casos se ignora y se obtiene del contexto.
THICKNESS  = 0.9;  // Grosor de las paredes del cilindro.
LID_HEIGHT = 6.6;  // Altura de la tapa plástica.
RADIUS     = 1.2;  // Radio del anillo de ajuste de la tapa
OFFSET     = 1.5;  // Distancia del borde al centro del anillo.
//---------------------------------------------------------------
/**
 * Dibuja la base del contenedor en 2D para que se extruya.
 *
 * @param {Float} length    Longitud del contenedor (eje Z).
 * @param {Float} thickness Grosor de las paredes del contenedor.
 */
module base(length = LENGTH, thickness = THICKNESS)
{
    _inner  = INNER / 2;
    _outer  = OUTER / 2;
    _i      = _inner - 0.5;
    _side   = _outer - _i;
    _b      = (OUTER - RADIUS - INNER) / 8;
    _h      = norm([ _b, _b ]);

    _p1     = [ _i, 0 ];
    _p2     = [ _outer, length ];
    _co     = [ _outer, _side ];
    _pa     = splice([ 0, 0 ], _co, _p1, _b, -1.08);
    _pb     = splice(_p1, _p2, _co, _b, -1.08);
    _points =  [
        _pa[0],
        _pa[1],
        _pa[2],
        _pb[0],
        _pb[1],
        _pb[2],
        [ _outer              , length - LID_HEIGHT - _b ], // 6
        [ _outer - _b         , length - LID_HEIGHT - _b ], // 7
        [ _outer - _b         , length - LID_HEIGHT      ], // 8
        [ _outer - RADIUS     , length - LID_HEIGHT      ], // 9
        [ _outer - RADIUS     , length - _b              ], // 10
        [ _outer - RADIUS - _b, length - _b              ], // 11
        [ _outer - RADIUS - _b, length                   ], // 12
        [ _inner              , length                   ], // 13
        [ _inner + _b         , length                   ], // 14
        [ _inner + _b         , length - _b              ], // 15
        [ _inner              , length - _b              ], // 16
        [ _inner              , thickness * 2            ], // 17
        [ 0                   , thickness * 2            ], // 18
        [ 0                   , 0                        ]  // 19
    ];
    polygon(points = _points);
    difference()
    {
        // Anillo para que la tapa ajuste.
        translate([ _outer - RADIUS, length - RADIUS - OFFSET ])
        {
            circle(r = RADIUS);
        }
        translate([ _inner - RADIUS, length - 2 * RADIUS - OFFSET ])
        {
            square([ RADIUS, 2 * RADIUS ]);
        }
    }
    for (_p = [ 1, 4, 7, 11, 15 ])
    {
        translate(_points[_p])
        {
            circle(r = _b);
        }
    }
}


/**
 * Dibuja el contenedor principal.
 *
 * @param {Float} length Longitud del contenedor (eje Z).
 */
module container(length = LENGTH)
{
    _2t = 2 * THICKNESS;
    rotate_extrude(convexity = 10)
    {
        base(length + _2t);
    }
    translate([ 0, 0, _2t ])
    {
        children();
    }
}

/**
 * Dibuja la tapa del contenedor si no se quiere usar la de
 * la lata de Pringles.
 */
module lid()
{
    _inner = INNER / 2;
    _outer = OUTER / 2;
    _r     = THICKNESS / 3;
    rotate_extrude(convexity = 10)
    {
        difference()
        {
            offset(r = -_r)
            {
                offset(delta = _r)
                {
                    translate([ 0, LENGTH ])
                    {
                        boxRounded2d(_outer, THICKNESS, [ 0, _r, 0, 0 ]);
                        translate([ _outer - THICKNESS, - LID_HEIGHT + _r ])
                        {
                            boxRounded2d(THICKNESS, LID_HEIGHT - _r, [ 0, 0, _r, _r ]);
                        }
                    }
                }
            }
            translate([ _outer - RADIUS - THICKNESS / 2, LENGTH - RADIUS - OFFSET ])
            {
                circle(r = RADIUS + 0.3);
            }
        }
    }
}

//-----------------------------------------------------------------------------
// Módulos para usar los submódulos de `Cylinder/children`.
// Ver la documentación de dichos módulos para conocer los parámetros y
// sus valores esperados.
//-----------------------------------------------------------------------------

module batteries(model = "AAA", padding = THICKNESS, tolerance = 0.6, from = 0)
{
    _length = getBattery(model)[2];
    container(_length)
    {
        cylinderBatteries(
            diameter  = INNER + 0.01,
            thickness = padding,
            margin    = _length * 0.5,
            model     = model,
            tolerance = tolerance,
            from      = from,
            name      = "Pringles-batteries"
        );
    }
}

module bits(length = 25.4, padding = 1, size = 1/4, tolerance = 0.25, type = "rect", text = "")
{
    difference()
    {
        container(length + padding)
        {
            cylinderBits(
                diameter  = INNER + 0.01,
                length    = 10,
                thickness = padding,
                size      = size,
                type      = type,
                tolerance = tolerance,
                name      = "Pringles-bits"
            );
        }
        if (text)
        {
            cylinderText(OUTER, 0.6667 * length, text, thickness = 0.4);
        }
    }
}

module memory(tolerance = 0.6, rotateSd = true, skipSd = [], rotateMicroSd = true, skipMicroSd = [], margin = 3)
{
    _microSd = getMicroSdSize(tolerance = tolerance);
    _sd      = getSdSize(tolerance = tolerance);
    _length  = max(skipMicroSd == true ? 0 : _microSd[1], skipSd == true ? 0 : _sd[1]);
    container(_length + margin + THICKNESS)
    {
        cylinderMemory(
            diameter      = INNER + 0.01,
            thickness     = 1,
            tolerance     = tolerance,
            rotateSd      = rotateSd,
            skipSd        = skipSd,
            rotateMicroSd = rotateMicroSd,
            skipMicroSd   = skipMicroSd,
            margin        = margin,
            name          = "Pringles-memory"
        );
    }
}

module segments(count = 3, length = LENGTH)
{
    container(length)
    {
        cylinderSegments(
            diameter  = INNER + 0.01,
            length    = length,
            thickness = THICKNESS,
            count     = count,
            name      = "Pringles-segments"
        );
    }
}

//-----------------------------------------------------------------------------
// Ejemplos de uso.
//-----------------------------------------------------------------------------
// Vacío:
// container(LENGTH);

// Cuadrados distribuidos en un patrón rectangular.
// container(LENGTH)
// {
//     cylinderShapeRectangular(INNER + 0.01, LENGTH / 2, width = 9, height = 9)
//     {
//        square(9, center = true);
//     }
// }

// Círculos distribuidos en un patrón rectangular:
// container(LENGTH)
// {
//     cylinderShapeRectangular(INNER + 0.01, LENGTH / 2, width = 9, height = 9)
//     {
//        circle(d = 9);
//     }
// }

// Cículos distribuidos en un patrón concéntrico.
// container(LENGTH)
// {
//     cylinderShapeConcentric(INNER + 0.01, LENGTH / 2, width = 9, height = 9)
//     {
//        circle(d = 9);
//     }
// }

// Hexágonos distribuidos en un patrón concéntrico.
// container(LENGTH)
// {
//     _d = 25.4 / 4;
//     cylinderShapeConcentric(INNER + 0.01, LENGTH / 2, width = _d, height = _d, thickness = 1.5)
//     {
//        circle(d = _d, $fn = 6);
//     }
// }

// Arcos o cilindros concéntricos.
// container(LENGTH)
// {
//     cylinderArcsConcentric(INNER + 0.01, LENGTH, count = 4);
// }

// Contenedor para pilas.
// batteries();

// Contenedor para memorias SD y MicroSD.
// memory(
//     skipMicroSd = [ -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 ],
//     rotateSd    = false,
//     skipSd      = []
// );

// Contenedor para puntas de taladro.
// bits(tolerance = 0.25, text = "HEXA");

// Contenedor dividido en 4 segmentos radiales.
// segments(4, length = 12.5);

//-----------------------------------------------------------------------------
// Tapa
//-----------------------------------------------------------------------------
// rotate([180, 0, 0])
// {
//     translate([ 0, 0, -LENGTH ])
//     {
//         lid();
//     }
// }
