/**
 * Dibuja un carrete genérico con diversos propósitos.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Home/UniversalSpool.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:3118330
 */
//-----------------------------------------------------------------------------
use <../Functions/Math/angles.scad>
use <../Modules/Cylinder/hollow.scad>
use <../Modules/Cylinder/spool.scad>
use <../Modules/Shapes/oval.scad>
//-----------------------------------------------------------------------------
/**
 * Dibuja la base del carrete.
 *
 * @param {Float}   outer     Diámetro externo de la base.
 * @param {Float}   inner     Diámetro interno de la base.
 * @param {Float}   thickness Grosor de la base.
 * @param {Float}   rod       Diámetro del tornillo central del carrete.
 * @param {Float[]} angles    Ángulos para dibujar las líneas de unión.
 */
module base(outer, inner, thickness, rod, angles)
{
    difference()
    {
        union()
        {
            cylinderHollow(outer, thickness, (outer - inner) / 2);
            tube(inner, thickness, thickness, rod, angles);
        }
        if (angles)
        {
            _l = outer - inner;
            _r = min(outer - inner, thickness * 2);
            _w = (_l - 4 * thickness) / 2;
            if (_l > 4 * _r + _w)
            {
                for (angle = angles)
                {
                    rotate(angle)
                    {
                        translate([ (outer - _w) / 2 - thickness, 0, - 0.01 ])
                        {
                            oval(_r, _w, thickness + 0.02);
                        }
                    }
                }
            }
        }
    }
}

/**
 * Dibuja el carrete completo.
 *
 * @param {Float} bottom    Diámetro de la base inferior (0 para no dibujarla).
 * @param {Float} lines     Cantidad de líneas a usar para unir el tornillo con el tubo.
 * @param {Float} diameter  Diámetro externo del carrete.
 * @param {Float} height    Altura del tubo (0 para no dibujar el tubo).
 * @param {Float} notch     Grosor de la muesca para meter el papel (0 deshabilita la muesca).
 * @param {Float} rod       Diámetro del tornillo central (0 para eliminarlo).
 * @param {Float} thickness Grosor de las paredes.
 * @param {Float} top       Diámetro de la base superior (0 para no dibujarla).
 */
module universalSpool(diameter = 21, height = 42, rod = 8, thickness = 1.5, notch = 1.2, lines = 3, bottom = 0, top = 0)
{
    _angles = lines > 0
        ? angles(360, 360 / lines)
        : [];

    // Dibujo de la base
    if (bottom > diameter)
    {
        translate([ 0, 0, - thickness ])
        {
            base(bottom, diameter, thickness, rod, _angles);
        }
    }

    // Dibujo de la tapa
    if (top > diameter)
    {
        translate([ 0, 0, height ])
        {
            base(top, diameter, thickness, rod, _angles);
        }
    }

    // Dibujo del tubo
    if (height)
    {
        tube(diameter, height, thickness, rod, _angles, notch);
    }
}

/**
 * Dibuja el tubo donde se introduce el carrete.
 *
 * @param {Float}   diameter  Diámetro del carrete.
 * @param {Float}   height    Altura del carrete.
 * @param {Float}   thickness Grosor de las paredes.
 * @param {Float}   rod       Diámetro del tornillo central del carrete.
 * @param {Float}   angles    Ángulos para dibujar las líneas de unión.
 * @param {Boolean} notch     Indica si se extraen las muescas para introducir el papel.
 */
module tube(diameter, height, thickness, rod, angles, notch)
{
    difference()
    {
        spool(diameter, height, thickness, rod, angles);
        if (notch)
        {
            _angle = (angles ? min(angles) : 0) + 15;
            rotate(_angle)
            {
                translate([ diameter / 2 - thickness * 1.5, 0, - 0.001 ])
                {
                    cube([ thickness * 2, notch, height + 0.002 ]);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------
// Inicio del script
//-----------------------------------------------------------------------------
$fn = $preview ? 90 : 180;

//universalSpool();
//universalSpool(bottom = 50, top = 50);