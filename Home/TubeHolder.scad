/**
 * Genera un soporte para colocar un tubo como los usados en los armarios.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Home/TubeHolder.scad
 * @license CC-BY-NC-4.0
 * @see     http://www.thingiverse.com/thing:3165425
 */
//-----------------------------------------------------------------------------
use <../Modules/Box/rounded.scad>
use <../Modules/screw.scad>
//-----------------------------------------------------------------------------
/**
 * Dibuja un cilindro con borde redondeado para introducir el tubo en algunos tipos de soportes.
 *
 * @param {Float} width     Ancho del soporte (eje X).
 * @param {Float} base      Altura donde se colocará el tubo (eje Y).
 * @param {Float} diameter  Diámetro del tubo.
 * @param {Float} thickness Grosor de la paredes del cilindro.
 * @param {Float} radius    Radio a usar para redondear el cilindro.
 */
module tubeHolder(width, base, height, diameter, thickness, radius = 1.2)
{
    _d = diameter + 2 * thickness;
    _r = min(thickness / 4, radius);
    rotate([ 90, 0, 180 ])
    {
        translate([ 0, _d / 2, 0 ])
        {
            difference()
            {
                union()
                {
                    hull()
                    {
                        for (_x = [ -1, 1 ])
                        {
                            translate([ _x * (width - _r) / 2, - _d / 2, 0 ])
                            {
                                cylinder(d = _r, h = height, center = true);
                            }
                        }
                        cylinder(d = _d, h = height, center = true);
                    }
                }
                cylinder(d = _d, h = height + 0.01);
            }
            rotate_extrude(angle = 360, convexity = 10)
            {
                translate([ diameter / 2, 0 ])
                {
                    square([ thickness, (base + thickness) / 2 ]);
                    translate([ thickness / 2, (base + thickness) / 2 ])
                    {
                        circle(d = thickness);
                    }
                }
            }
        }
    }
}

/**
 * Dibuja un soporte de tipo bloque sobre el que se coloca el tubo.
 *
 * @param {Float}  diameter  Diámetro del tubo.
 * @param {Float}  height    Altura de la pieza (eje Y).
 * @param {Float}  radius    Radio de las equinas.
 * @param {Float}  screw     Diámetro del tornillo a usar.
 * @param {Float}  thickness Grosor de la pieza (eje Z).
 * @param {Float}  width     Anchura de la pieza (eje X).
 * @param {String} headType  Diferentes tipos de cabeza del tornillo:
 *                           - false : Ninguna
 *                           - hex   : Hexagonal
 *                           - flat  : Avellanada
 *                           - cyl   : Cilíndrica
 */
module tubeHolderBlock(diameter = 25.4, height = 48, radius = 2, screw = 5, thickness = 12, width = 55, headType = "flat")
{
    _margin = diameter / 4;       // Margen entre el tubo y el borde superior.
    _rtube  = (diameter + 2) / 2; // Radio del tubo. Sumamos 2 al diámetro para tener holgura.
    difference()
    {
        // Base
        hull()
        {
            for (_x = [ radius, width - radius ])
            {
                for (_y = [ radius, height - radius ])
                {
                    translate([ _x, _y, 0 ])
                    {
                        cylinder(r = radius, h = thickness, center = true);
                    }
                }
            }
        }
        // Ranura para el tubo
        translate([ width / 2, height - _margin, 0 ])
        {
            cube([ _rtube * 2, _rtube * 2, thickness + eps ], center = true);
            translate([ 0, - _rtube, 0 ])
            {
                cylinder(r = _rtube, h = thickness + eps, center = true);
            }
        }
        // Redondeo de las esquinas internas.
        for (_x = [ -1, 1 ])
        {
            translate([
                width / 2 + _x * (_rtube + radius - eps),
                height - radius,
                0
            ])
            {
                difference()
                {
                    translate([ - _x * radius / 2, radius / 2, 0 ])
                    {
                        cube([ radius, radius, thickness + eps ], center = true);
                    }
                    cylinder(r = radius, h = thickness + eps, center = true);
                }
            }
        }
        if (headType)
        {
            _w = width / 2 - _rtube;
            tubeHolderScrews(
                [
                    [ _w / 2,         height * 0.8, 0 ],
                    [ width - _w / 2, height * 0.8, 0 ],
                    [ width / 2,      (height - _margin) * 0.5 - _rtube, 0 ]
                ],
                thickness,
                0.25 * thickness,
                screw,
                headType
            );
        }
    }
}

/**
 * Dibuja un soporte acodado 90º con los tornillos por delante.
 *
 * @param {Float}  length    Longitud del soporte desde la base al centro del tubo (eje Z).
 * @param {Float}  width     Ancho del soporte (eje X).
 * @param {Float}  base      Base del soporte (eje Y).
 * @param {Float}  diameter  Diámetro del tubo.
 * @param {Float}  screw     Diámetro de los tornillos.
 * @param {Float}  thickness Grosor de las pletinas.
 * @param {Float}  radius    Radio a usar para redondear algunos bordes.
 * @param {String} headType  Diferentes tipos de cabeza del tornillo:
 *                           - false : Ninguna
 *                           - hex   : Hexagonal
 *                           - flat  : Avellanada
 *                           - cyl   : Cilíndrica
 */
module tubeHolderType1(length = undef, width = undef, base = undef, diameter = 8, screw = 3.5, thickness = 5, radius = 1.2, headType = "flat")
{
    assert(thickness > 2 * radius, "El grosor debe ser mayor a 2 * radius");
    _t = 2 * thickness;
    _b = base == undef ? screw + 2 * _t : base;
    _d = screw + _t;
    _l = length == undef
        ? diameter + thickness
        : length;
    _w = width == undef
        ? diameter + _t + 2 * _d
        : width;
    _z = _l - _d / 2 - thickness;
    echo(_z, thickness, radius);
    assert(_z >= thickness + radius, "La proporción entre length, diameter y thickness es incorrecta");
    translate([ _w / 2, 0, 0 ])
    {
        difference()
        {
            rotate([ 0, - 90, 0 ])
            {
                linear_extrude(_w, convexity = 10)
                {
                    offset(r = - radius)
                    {
                        offset(delta = radius)
                        {
                            offset(r = radius)
                            {
                                offset(delta = - radius)
                                {
                                    square([ _z,  thickness ]);
                                    square([ thickness, _b ]);
                                }
                            }
                        }
                    }
                    translate([ _z - 2 * radius, 0 ])
                    {
                        square([ 2 * radius, thickness ]);
                    }
                }
            }
            tubeHolderScrews(
                [
                    for (_x = [ (_d + thickness) / 2, _w - (_d + thickness) / 2 ])
                        [ - _x, (_b + thickness) / 2, thickness / 2 ]
                ],
                thickness,
                thickness / 2,
                screw,
                headType
            );
        }
    }
    translate([ 0, thickness / 2, _z ])
    {
        tubeHolder(_w, _b, thickness, diameter, thickness, radius);
    }
}


/**
 * Dibuja un soporte con la línea de los tornillos perpendicular al tubo.
 *
 * @param {Float}  length    Longitud del soporte desde la base al centro del tubo (eje Z).
 * @param {Float}  width     Ancho del soporte (eje X).
 * @param {Float}  diameter  Diámetro del tubo.
 * @param {Float}  screw     Diámetro de los tornillos.
 * @param {Float}  thickness Grosor de las pletinas.
 * @param {Float}  radius    Radio a usar para redondear algunos bordes.
 * @param {String} headType  Diferentes tipos de cabeza del tornillo:
 *                           - false : Ninguna
 *                           - hex   : Hexagonal
 *                           - flat  : Avellanada
 *                           - cyl   : Cilíndrica
 */
module tubeHolderType2(length, width, diameter = 8, screw = 3.5, thickness = 5, radius = 1.2, headType = "flat")
{
    _t = 2 * thickness;
    _d = screw + _t;
    _l = length == undef
        ? diameter + thickness
        : length;
    _w = width == undef
        ? diameter + _t + 2 * _d
        : width;
    _z = _l - _d / 2 - _t - thickness;
    _y = _d + _t;
    echo(_z);
    assert(_z >= thickness, "La proporción entre length, diameter y thickness es incorrecta");
    translate([ 0, 0, _z ])
    {
        tubeHolder(_w, 2 * _d, _y, diameter, thickness, radius);
    }
    translate([ 0, 0, _z / 2 ])
    {
        cube([ _w, _y, _z ], center = true);
    }
    translate([ 0, 0, _t / 2 ])
    {
        difference()
        {
            _ww = _w + 2 * _d + _t;
            boxRounded(_ww, _y, _t, radius);
            tubeHolderScrews(
                [
                    for (_x = [ -1, 1 ])
                        [ - _x * (_ww - _d - thickness) / 2, 0, 0 ]
                ],
                _t,
                thickness,
                screw,
                headType
            );
        }
    }
}

/**
 * Dibuja un tornillo para sacar su volumen de la base del soporte.
 *
 * @param {Float[][]} points   Coordenadas donde se colocarán los tornillos.
 * @param {Float}     depth    Profundidad de agujero a dejar para la cabeza del tornillo.
 * @param {Float}     length   Longitud del tornillo.
 * @param {Float}     screw    Diámetro del tornillo.
 * @param {String}    headType Diferentes tipos de cabeza del tornillo:
 *                             - false : Ninguna
 *                             - hex   : Hexagonal
 *                             - flat  : Avellanada
 *                             - cyl   : Cilíndrica
 */
module tubeHolderScrews(points, length, depth, screw, headType = "flat")
{
    assert(depth < length, "La profundidad debe ser menor que el largo del tornillo");
    screwHoles(
        points,
        length + eps,
        screw,
        [ 2 * screw, headType == "flat" ? 90 : depth ],
        headType
    );
}

//------------------------------------------
// Inicio del script
//------------------------------------------
eps = 0.001;
$fn = $preview ? 75 : 180;
//
//tubeHolderBlock();
//tubeHolderType1(15, 35, diameter = 10, thickness = 3);
//tubeHolderType2(18, 20, diameter = 10, thickness = 3);
//tubeHolderScrews([[0,0,0]], 6, 1, 5);