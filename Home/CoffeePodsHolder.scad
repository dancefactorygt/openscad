/**
 * Soporte para cápsulas de café.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Home/CoffeePodsHolder.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:5144085
 */
//-----------------------------------------------------------------------------
use <../Modules/Box/rounded.scad>
use <../Modules/screw.scad>
//-----------------------------------------------------------------------------
// Valores personalizables
//-----------------------------------------------------------------------------
count  =   2;   // Cantidad de cápsulas a almacenar.
inner  =  48.8; // Diámetro interior de la cápsula.
nail   =  10.0; // Longitud del clavo de 1mm a usar para unir las piezas.
outer  =  55.4; // Diámetro exterior de la cápsula. Las Dolce Gusto van desde 52 hasta 55 dependiendo del fabricante.
radius =   0.6; // Radio para redondear los bordes.
screw  =   3.2; // Diámetro del tornillo a usar para fijar los soportes.
//-----------------------------------------------------------------------------
/**
 * Dibuja el soporte.
 *
 * (-d,2d+t) ----- (d,2d+t)
 *         |    /
 *         |   /
 *         |  / (0,d+t)
 *         |  |
 *         |  \ (0,d)
 *         |   \
 *         |    \
 *   (-d,0) ----- (d,0)
 *
 * @param {Float} diff      Diferencia entre el diámetro externo y el interno.
 * @param {Float} thickness Grosor de la pestaña que entra en el soporte.
 * @param {Float} radius    Radio para redondear los bordes.
 */
module holder(diff, radius = 0.6, thickness = 1)
{
    _x1 = diff;
    _x2 = diff + 2 * radius; // Sumamos radius para compesar el redondeo
    _y1 = diff;
    _y2 = diff + thickness;
    _y3 = _y2 + diff;
    offset(r = radius)
    {
        offset(delta = - radius)
        {
            polygon(
                points = [
                    [ - _x1, 0   ],
                    [ - _x1, _y3 ],
                    [   _x2, _y3 ],
                    [     0, _y2 ],
                    [     0, _y1 ],
                    [   _x2, 0   ]
                ]
            );
        }
    }
}

module pod(outer, inner, screw, radius)
{
    _diff = outer - inner;
    _t    = min(0.375 * (_diff + radius / 2), 2.4);
    difference()
    {
        rotate([ 90, 0, 0 ])
        {
            linear_extrude(outer, convexity = 10)
            {
                for (_x = [ - 0.5, 0.5 ])
                {
                    translate([ _x * outer, 0, 0 ])
                    {
                        mirror([ _x > 0 ? 1 : 0, 0, 0 ])
                        {
                            holder(_diff / 2, radius, 0.8);
                        }
                    }
                }
                translate([ - outer / 2, 0 ])
                {
                    square([ outer, _t ]);
                }
            }
        }
        for (_y = [ 0, - outer ])
        {
            translate([ 0, _y, 0 ])
            {
                cylinder(d = inner - screw, h = 2 * diff, center = true);
            }
        }
        translate([ 0, - outer / 2, _t / 2 ])
        {
            screwFlatHead(_t + 0.01, screw, [], 0.3);
        }
    }
}

module test(outer, inner, screw, radius)
{
    diff = (outer - inner) / 2;
    intersection()
    {
        pod(outer, inner, screw, radius);
        translate([ 0, - outer / 2, diff ])
        {
            cube([ 2 * outer, 4 * screw, 4 * diff ], center = true);
        }
    }
}
//-----------------------------------------------------------------------------
// Inicio del script
//-----------------------------------------------------------------------------
$fn       = $preview ? 30 : 180;
diff      = (outer - inner) / 2;
length    = count * outer;
//

// test(outer, inner, screw, radius);
difference()
{
    for (y = [ 1 : count ])
    {
        translate([ 0, y * outer, 0 ])
        {
            pod(outer, inner, screw, radius);
        }
    }
    // Agujeros para los clavos de empalme.
    if (nail > 0)
    {
        for (y = [ - 0.01, length - nail / 2 + 0.01 ])
        {
            for (x = [ - 0.5, 0.5 ])
            {
                translate([ x * (outer + diff), y, diff + radius / 2 ])
                {
                    rotate([ - 90, 0, 0 ])
                    {
                        cylinder(d = 1.4, h = nail / 2);
                    }
                }
            }
        }
    }
}
